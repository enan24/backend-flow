# BackEnd FLOW

## Configuración de variables e inicio de API

Para iniciar la API es necesario configurar las variables de entorno. Para esto, debemos duplicar el archivo `.env.dist` y poner el nombre `.env`.

### Variables

- **PORT** Puerto en el que se va a iniciar la API, por defecto será el puerto 3000
- **URL_IP_API_JSON** URL de la API para obtener la geolocalización del usuario a traves de la IP
- **URL_API_OPEN_GEOCODING** URL de la API para obtener la geolocalización a traves del nombre de una ciudad
- **URL_API_OPEN_WEATHER_MAP** URL de la API para obtener los datos del clima
- **KEY_OPEN_WEATHER_MAP** Key para acceder a la API de Open Weather y Open Geocoding

### Instalación de librerías

Se deben instalar todas las librerías que requiere el proyecto, se debe ejecutar en la consola el comando `npm install`

### Inicio de la API

Luego de realizar la configuración de las variables y de instalar las librerías necesarias, para iniciar el proyecto se debe ejecutar en la consola el comando `npm start`

## Ejecutar casos de prueba

Para ejecutar los casos de prueba solo se debe ejecutar en la consola el comando `npm run test`

## APIs Externas utilizadas

En el proyecto se utilizaron las siguientes APIs externas
- http://ip-api.com/json/
    - API Para obtener la geolocalización de una IP
- http://api.openweathermap.org/geo/1.0/
    - API Para obtener la geolocalización de una ciudad por su nombre
- https://api.openweathermap.org/data/2.5/
    - API Para obtener los datos del clima utilizando las coordenadas del lugar