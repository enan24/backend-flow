import buildApp from '../../app.js';
import { test } from 'tap';
import nock from 'nock';
const {
	URL_IP_API_JSON,
	URL_API_OPEN_WEATHER_MAP,
	KEY_OPEN_WEATHER_MAP,
	URL_API_OPEN_GEOCODING
} = process.env;

test('GET /api/v1/current/ - Should return error "city not found" when send TestCity', (test) => {
	const app = buildApp();

	test.teardown(() => app.close());
	app.inject({
		method: 'GET',
		url: 'api/v1/current/TestCity'
	})
		.then((response) => {
			test.equal(response.statusCode, 404);
			const data = response.json();
			test.has(data, {
				message: 'City not found',
				code: 'CITY_NOT_FOUND'
			});
			test.end();
		});
});

test('GET /api/v1/current/ - Should return weather', (test) => {
	const app = buildApp();

	const dataWeatherApi = {
		coord: {
			lon: -58.4944,
			lat: -34.6805
		},
		weather: [{
			id: 800,
			main: 'Clear',
			description: 'clear sky'
		}],
		wind: {
			speed: 5.66,
			deg: 90
		},
		name: 'Mataderos',
		cod: 200
	};

	const dataLocation = {
		name: 'Lomas del Mirador',
		lat: -34.6660241,
		lon: -58.524548,
		country: 'AR',
		state: 'Buenos Aires'
	};

	nock(`${URL_IP_API_JSON}`)
		.persist()
		.get('/181.45.152.42')
		.reply(200, dataLocation);

	nock(`${URL_API_OPEN_WEATHER_MAP}`)
		.persist()
		.get('/weather')
		.query({
			lat: -34.6660241,
			lon: -58.524548,
			appid: KEY_OPEN_WEATHER_MAP
		})
		.reply(200, dataWeatherApi);

	test.teardown(() => app.close());
	app.inject({
		method: 'GET',
		headers: {
			'Remote-Addr': '181.45.152.42',
			'X-Forwaded-For': '181.45.152.42'
		},
		url: 'api/v1/current/'
	})
		.then((response) => {
			test.equal(response.statusCode, 200);
			const data = response.json();
			test.ok(data, {
				weather: dataWeatherApi,
				dataLocation
			});
			nock.cleanAll();
			test.end();
		});
});

test('GET /api/v1/current/ - Should return weather when send "Buenos Aires"', (test) => {
	const app = buildApp();

	const dataWeatherApi = {
		coord: {
			lat: -34.6075682,
			lon: -58.4370894
		},
		weather: [{
			id: 800,
			main: 'Clear',
			description: 'clear sky'
		}],
		wind: {
			speed: 6.17,
			deg: 90
		},
		name: 'Villa Crespo',
		cod: 200
	};

	const dataLocation = {
		name: 'Buenos Aires',
		lat: -34.6075682,
		lon: -58.4370894,
		country: 'AR',
		state: 'Autonomous City of Buenos Aires'
	};

	nock(`${URL_API_OPEN_GEOCODING}`)
		.persist()
		.get('/direct')
		.query({
			q: 'Buenos Aires',
			appid: KEY_OPEN_WEATHER_MAP
		})
		.reply(200, [dataLocation]);

	nock(`${URL_API_OPEN_WEATHER_MAP}`)
		.persist()
		.get('/weather')
		.query({
			lat: -34.6075682,
			lon: -58.4370894,
			appid: KEY_OPEN_WEATHER_MAP
		})
		.reply(200, dataWeatherApi);

	test.teardown(() => app.close());
	app.inject({
		method: 'GET',
		url: 'api/v1/current/Buenos Aires'
	})
		.then((response) => {
			test.equal(response.statusCode, 200);
			const data = response.json();
			test.has(data, {
				weather: dataWeatherApi,
				dataLocation
			});
			nock.cleanAll();
			test.end();
		});
});
