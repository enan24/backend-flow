import buildApp from '../../app.js';
import { test } from 'tap';
import nock from 'nock';
const { URL_IP_API_JSON } = process.env;

test('GET /api/v1/location - Should return data location with correct ip', (test) => {
	const app = buildApp();

	const dataIpApi = {
		status: 'success',
		country: 'Argentina',
		countryCode: 'AR',
		region: 'B',
		regionName: 'Buenos Aires',
		city: 'Villa Madero',
		zip: '1770',
		lat: -34.6805,
		lon: -58.4944,
		timezone: 'America/Argentina/Buenos_Aires',
		isp: 'Telecentro S.A.',
		org: 'Telecentro S.A',
		as: 'AS27747 Telecentro S.A.',
		query: '181.45.152.42'
	};

	nock(`${URL_IP_API_JSON}`)
		.persist()
		.get('/181.45.152.42')
		.reply(200, dataIpApi);

	test.teardown(() => app.close());
	app.inject({
		method: 'GET',
		headers: {
			'Remote-Addr': '181.45.152.42',
			'X-Forwaded-For': '181.45.152.42'
		},
		url: 'api/v1/location'
	})
		.then((response) => {
			test.equal(response.statusCode, 200);
			const data = response.json();
			test.has(data, dataIpApi);
			nock.cleanAll();
			test.end();
		});
});
