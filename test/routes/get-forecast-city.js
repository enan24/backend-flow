import buildApp from '../../app.js';
import { test } from 'tap';
import nock from 'nock';
const {
	URL_IP_API_JSON,
	URL_API_OPEN_WEATHER_MAP,
	KEY_OPEN_WEATHER_MAP,
	URL_API_OPEN_GEOCODING
} = process.env;

test('GET /api/v1/forecast/ - Should return error "city not found" when send TestCity', (test) => {
	const app = buildApp();

	test.teardown(() => app.close());
	app.inject({
		method: 'GET',
		url: 'api/v1/forecast/TestCity'
	})
		.then((response) => {
			test.equal(response.statusCode, 404);
			const data = response.json();
			test.has(data, {
				message: 'City not found',
				code: 'CITY_NOT_FOUND'
			});
			test.end();
		});
});

test('GET /api/v1/forecast/ - Should return forecast', (test) => {
	const app = buildApp();

	const dataWeatherApi = {
		cod: 200,
		cnt: 40,
		list: [{
			main: {
				temp: 285.55,
				feels_like: 284.65,
				temp_min: 284.22,
				temp_max: 285.55,
				pressure: 1020,
				sea_level: 1020,
				grnd_level: 1016,
				humidity: 69,
				temp_kf: 1.33
			}
		}, {
			main: {
				temp: 284.19,
				feels_like: 283.18,
				temp_min: 283.18,
				temp_max: 284.19,
				pressure: 1019,
				sea_level: 1019,
				grnd_level: 1016,
				humidity: 70,
				temp_kf: 1.01
			},
		}],
		city: {
			id: 3427470,
			name: 'Lomas del Mirador',
			coord: {
				lat: -34.666,
				lon: -58.5245
			},
			country: 'AR',
		}
	};

	const dataLocation = {
		name: 'Buenos Aires',
		lat: -34.6075682,
		lon: -58.4370894,
		country: 'AR',
		state: 'Autonomous City of Buenos Aires'
	};

	nock(`${URL_IP_API_JSON}`)
		.persist()
		.get('/181.45.152.42')
		.reply(200, dataLocation);

	nock(`${URL_API_OPEN_WEATHER_MAP}`)
		.persist()
		.get('/forecast')
		.query({
			lat: -34.6075682,
			lon: -58.4370894,
			appid: KEY_OPEN_WEATHER_MAP
		})
		.reply(200, dataWeatherApi);

	test.teardown(() => app.close());
	app.inject({
		method: 'GET',
		headers: {
			'Remote-Addr': '181.45.152.42',
			'X-Forwaded-For': '181.45.152.42'
		},
		url: 'api/v1/forecast/'
	})
		.then((response) => {
			test.equal(response.statusCode, 200);
			const data = response.json();
			test.has(data, {
				weather: dataWeatherApi,
				dataLocation
			});
			nock.cleanAll();
			test.end();
		});
});

test('GET /api/v1/forecast/ - Should return forecast when send "Lomas del Mirador"', (test) => {
	const app = buildApp();

	const dataWeatherApi = {
		cod: 200,
		cnt: 40,
		list: [{
			main: {
				temp: 285.55,
				feels_like: 284.65,
				temp_min: 284.22,
				temp_max: 285.55,
				pressure: 1020,
				sea_level: 1020,
				grnd_level: 1016,
				humidity: 69,
				temp_kf: 1.33
			}
		}, {
			main: {
				temp: 284.19,
				feels_like: 283.18,
				temp_min: 283.18,
				temp_max: 284.19,
				pressure: 1019,
				sea_level: 1019,
				grnd_level: 1016,
				humidity: 70,
				temp_kf: 1.01
			},
		}],
		city: {
			id: 3427470,
			name: 'Villa Ansaldi',
			coord: {
				lat: -34.666,
				lon: -58.5245
			},
			country: 'AR',
		}
	};

	const dataLocation = {
		name: 'Lomas del Mirador',
		lat: -34.6660241,
		lon: -58.524548,
		country: 'AR',
		state: 'Buenos Aires'
	};

	nock(`${URL_API_OPEN_GEOCODING}`)
		.persist()
		.get('/direct')
		.query({
			q: 'Lomas del Mirador',
			appid: KEY_OPEN_WEATHER_MAP
		})
		.reply(200, [dataLocation]);

	nock(`${URL_API_OPEN_WEATHER_MAP}`)
		.persist()
		.get('/forecast')
		.query({
			lat: -34.6660241,
			lon: -58.524548,
			appid: KEY_OPEN_WEATHER_MAP
		})
		.reply(200, dataWeatherApi);

	test.teardown(() => app.close());
	app.inject({
		method: 'GET',
		url: 'api/v1/forecast/Lomas del Mirador'
	})
		.then((response) => {
			test.equal(response.statusCode, 200);
			const data = response.json();
			test.ok(data, {
				weather: dataWeatherApi,
				dataLocation
			});
			nock.cleanAll();
			test.end();
		});
});