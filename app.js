import 'dotenv/config';
import fastify from 'fastify';
import routesV1 from './routes/v1/index.js';

export default function buildApp() {
	const app = fastify({
		logger: true,
		trustProxy: true
	});
	
	app.register(routesV1, { prefix: 'api/v1' });

	return app;
}
