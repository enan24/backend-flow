import { getLocationByIp } from '../../utils/ip-api/index.js';
import { getForecastByCoordinates, getLocationCity } from '../../utils/open-wheater/index.js';

function getDataLocation(req) {
	let promise;
	if (req.params && req.params.city) {
		promise = getLocationCity(req.params.city).then((res) => res[0]);
	} else {
		const ipAddress = req.headers['x-forwaded-for'] || req.ip;
		promise = getLocationByIp(ipAddress);
	}

	return promise
		.then((data) => {
			if (!data) {
				throw new Error('Coordinates not found');
			}
			const { lat, lon } = data;
			if (!lat || !lon) {
				throw new Error('Coordinates not found');
			}
			return data;
		});
}

export default function handleCurrentCity(request, reply) {
	let dataLocation;
	return getDataLocation(request)
		.then((data) => {
			const { lat, lon } = data; 
			dataLocation = data;
			return getForecastByCoordinates({ lat, lon });
		})
		.then((weather) => {
			return reply.code(200).send({
				weather,
				dataLocation
			});
		})
		.catch((err) => {
			if (err.message === 'Coordinates not found') {
				return reply.code(404).send({
					message: 'City not found',
					code: 'CITY_NOT_FOUND'
				});
			}
			return reply.code(500).send({
				message: 'Internal error',
				code: 'INTERNAL_ERROR'
			});
		});
}