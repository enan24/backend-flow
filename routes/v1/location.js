import { getLocationByIp } from '../../utils/ip-api/index.js';

export default function(request, reply) {
	const ipAddress = request.headers['x-forwaded-for'] || request.ip;
	return getLocationByIp(ipAddress)
		.then((location) => {
			return reply.code(200).send(location);
		})
		.catch(() => {
			return reply.code(500).send({
				message: 'Internal error',
				code: 'INTERNAL_ERROR'
			});
		});
}