import handleLocation from './location.js';
import handleCurrentCity from './handle-current-city.js';
import handleForecastCity from './handle-forecast-city.js';

export default (fastify, opts, done) => {
	fastify.get('/location', handleLocation);
	fastify.get('/current/:city', handleCurrentCity);
	fastify.get('/forecast/:city', handleForecastCity);
	done();
};
