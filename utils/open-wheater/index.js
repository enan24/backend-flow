import fetch from 'node-fetch';

const {
	URL_API_OPEN_WEATHER_MAP,
	KEY_OPEN_WEATHER_MAP,
	URL_API_OPEN_GEOCODING
} = process.env;

export function getLocationCity(city) {
	return fetch(`${URL_API_OPEN_GEOCODING}direct?q=${city}&appid=${KEY_OPEN_WEATHER_MAP}`)
		.then((response) => response.json());
}

export function getWeatherByCoordinates(coordinates) {
	return fetch(`${URL_API_OPEN_WEATHER_MAP}weather?lat=${coordinates.lat}&lon=${coordinates.lon}&appid=${KEY_OPEN_WEATHER_MAP}`)
		.then((response) => response.json());
}

export function getForecastByCoordinates(coordinates) {
	return fetch(`${URL_API_OPEN_WEATHER_MAP}forecast?lat=${coordinates.lat}&lon=${coordinates.lon}&appid=${KEY_OPEN_WEATHER_MAP}`)
		.then((response) => response.json());
}