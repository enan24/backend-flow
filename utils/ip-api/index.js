import fetch from 'node-fetch';

const { URL_IP_API_JSON } = process.env;

export function getLocationByIp(ipAddress) {
	return fetch(`${URL_IP_API_JSON}${ipAddress}`)
		.then((response) => response.json());
}
